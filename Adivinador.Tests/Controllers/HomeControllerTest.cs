﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Adivinador;
using Adivinador.Controllers;

namespace Adivinador.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Inicio()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Inicio() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
