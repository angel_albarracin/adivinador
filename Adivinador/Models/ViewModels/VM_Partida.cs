﻿using System.Collections.Generic;

namespace Adivinador.Models.ViewModels
{
    public class VM_Partida
    {
        private Jugador Player { get; set; }
        private Partida Game { get; set; }

        public string PlayerName { get{ return Player.Nombre; } }

        public string TriedNumbers { get { return Game.IntentosTxt(); } }

        public string StatusMessage { get { return Game.GetMensaje(); } }

        public int Status { get { return Game.EstadoInt; } }

        public bool IsEndGame { get { return Game.FinJuego(); } }

        public int RemainingAttempts { get { return 10 - Game.Intentos.Count; } }

        public VM_Partida(Jugador j, Partida p)
        {
            Player = j;
            Game = p;
        }

        public void UpdateGame(int number)
        {
            Game.Probar(number);
            Game.Intentos.Add(number);
        }

    }
}