﻿using System.Collections.Generic;

namespace Adivinador.Models.ViewModels
{
    public class VM_Puntuaciones
    {
        private Jugador JSesion { get; set; }
        private Jugador JSesionCookie { get; set; }
        private Jugador JCookie { get; set; }

        public VM_Puntuaciones(Jugador sesion, Jugador sesionCookie, Jugador cookie)
        {
            JSesion = sesion;
            JSesionCookie = sesionCookie;
            JCookie = cookie;
        }

        public string BestScoreSession { get { return JSesion.ToString(); } }
        public string BestBrowserScoreUser { get { return JSesionCookie.ToString(); } }
        public string BestBrowserScore{ get { return JCookie.ToString(); } }
        public string BestSistScore { get { return Juego.Instance.BestScoreSistTxt; } }
    }
}