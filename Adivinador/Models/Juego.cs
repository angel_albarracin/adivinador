﻿using System.Collections.Generic;

namespace Adivinador.Models
{
    public sealed class Juego
    {
        public List<Jugador> Records { get; set; }
        public Jugador RecordSist { get; set; }
        public int BestScoreSistInt
        {
            get
            {
                return RecordSist.Record();
            }
        }
        public string BestScoreSistTxt

        {
            get
            {
                return RecordSist.ToString();
            }
        }



        private Juego()
        {
            Records = new List<Jugador>();
            RecordSist = null;
        }

        private readonly static Juego _instance = new Juego();

        public static Juego Instance
        {
            get { return _instance; }
        }

        public void NewPlayer(Jugador j)
        {
            var jugador = new Jugador()
            {
                Nombre = j.Nombre,
                Puntos = j.Puntos,
                PartidasJugadas = j.PartidasJugadas
            };
            Records.Add(jugador);
            RecordSist = RecordSist ?? j;
        }

        public bool IsNewPlayer(Jugador j)
        {
            return !Records.Exists(x => x.Nombre == j.Nombre);
        }

        public void NewRecordSist(Jugador j)
        {
            RecordSist = j;
        }

        public void UpdateRecordSist()
        {
            var aux = new Jugador();
            Records.ForEach(x => {
                if (x.Record() > aux.Record())
                    aux = x;
            });
            RecordSist = aux;
        }

        public void UpdateScorePlayer(Jugador jugador, int estado, int restantes)
        {
            var recordJugador = Records.Find(x => x.Nombre == jugador.Nombre);
            recordJugador.Update(estado, restantes);
        }

    }
}