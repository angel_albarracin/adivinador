﻿using Newtonsoft.Json;
using System.Web;

namespace Adivinador.Models
{
    public class Jugador
    {
        public string Nombre { get; set; }
        public int Puntos { get; set; }
        public int PartidasJugadas { get; set; }

        public Jugador()
        {
            Nombre = "";
            Puntos = 0;
            PartidasJugadas = 0;
        }

        public void Update(int estado, int restantes)
        {
            PartidasJugadas++;
            Puntos += estado;
        }

        public int Record()
        {
            return Puntos;
        }

        public override string ToString()
        {
            return $"{Nombre} {Record()}";
        }

    }
}