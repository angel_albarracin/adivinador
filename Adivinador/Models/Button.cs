﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adivinador.Models
{
    public class Button
    {
        public string Key { get; set; }
        public string Class { get; set; }
        public string Value { get; set; }

        public Button(string key, string value, string classCss)
        {
            Key = key;
            Class = classCss;
            Value = value;
        }
    }
}