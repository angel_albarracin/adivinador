﻿using System;
using System.Collections.Generic;

namespace Adivinador.Models
{
    public enum Estado : int { Iniciada = -1, Perdida, Ganada, NumMayor, NumMenor }

    public class Partida
    {
        
        public List<int> Intentos { get; set; }
        public int NSecreto { get; set; }
        public Estado Estado { get; set; }
        public int EstadoInt { get { return (int)Estado; } }

        public Partida()
        {
            NSecreto = Random();
            Intentos = new List<int>();
            Estado = Estado.Iniciada;
        }

        public void Probar(int numero)
        {
            if (numero > NSecreto)
                Estado = Estado.NumMayor;
            if (numero < NSecreto)
                Estado = Estado.NumMenor;
            if (Intentos.Count == 10)
                Estado = Estado.Perdida;
            if (numero == NSecreto)
                Estado = Estado.Ganada;
        }

        public string GetMensaje()
        {
            string mensaje = "";
            switch (Estado)
            {
                case Estado.Perdida:
                    mensaje = "Has Perdido";
                    break;
                case Estado.Ganada:
                    mensaje = "Has Ganado";
                    break;
                case Estado.NumMayor:
                    mensaje = "El numero secreto es menor";
                    break;
                case Estado.NumMenor:
                    mensaje = "El numero secreto es mayor";
                    break;
                default:
                    break;
            }
            return mensaje;
        }

        public string IntentosTxt()
        {
            string txt = "";
            Intentos.ForEach(i => {
                txt += $"{i.ToString()} ";
            });
            return txt;
        }

        private int Random()
        {
            return new Random().Next(1, 100);
        }

        public bool FinJuego()
        {
            return Estado == Estado.Perdida || Estado == Estado.Ganada;
        }

    }
}