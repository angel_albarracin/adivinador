﻿using Adivinador.Filters;
using Adivinador.Models;
using Adivinador.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace Adivinador.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Inicio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Inicio(string nick)
        {
            var jugador = new Jugador() { Nombre = nick };
            Session.Add("Jugador", jugador);
            if(Juego.Instance.IsNewPlayer(jugador))
                Juego.Instance.NewPlayer(jugador);
            return RedirectToAction("Puntuaciones");
        }

        [AuthFilter]
        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Inicio");
        }

        [AuthFilter]
        [HttpGet]
        public ActionResult Puntuaciones()
        {
            var jugador = (Jugador)Session["Jugador"];
            var puntuaciones = new VM_Puntuaciones(jugador, GetJugadorFromCookie(jugador.Nombre), GetJugadorFromCookie("RMaquina"));
            return View(puntuaciones);
        }

        [AuthFilter]
        [HttpGet]
        public ActionResult Jugar()
        {
            var partida = new Partida();
            Session.Add("Partida", partida);
            var partidaVM = new VM_Partida((Jugador)Session["Jugador"], partida);
            return View(partidaVM);
        }

        [AuthFilter]
        [HttpPost]
        public ActionResult Jugar(int numero)
        {
            var partidaVM = new VM_Partida((Jugador)Session["Jugador"], (Partida)Session["Partida"]);
            partidaVM.UpdateGame(numero);
            if (partidaVM.IsEndGame)
                AjustarPuntuaciones(partidaVM.Status, partidaVM.RemainingAttempts);
            return View(partidaVM);
        }

        private Jugador GetJugadorFromCookie(string key)
        {
            var cookie = Request.Cookies[key];
            var jugador = (Jugador)Session["Jugador"];
            if (cookie != null)
                jugador = JsonConvert.DeserializeObject<Jugador>(cookie.Value);
            else
                Response.Cookies[key].Value = JsonConvert.SerializeObject(jugador);
            return jugador;
        }

        private void SetJugadorForCookie(string key, Jugador j)
        {
            Response.Cookies[key].Value = JsonConvert.SerializeObject(j);
            Response.Cookies[key].Expires = DateTime.Now.AddHours(2);
        }

        /// <summary>
        /// La mejor puntuacion de session se actualiza siempre (partidas ganadas)
        /// </summary>
        private void AjustarPuntuaciones(int estado, int restantes)
        {
            var jugador = (Jugador)Session["Jugador"];
            jugador.Update(estado, restantes);
            var recordJugadorMaquina = GetJugadorFromCookie(jugador.Nombre);
            recordJugadorMaquina.Update(estado, restantes);
            SetJugadorForCookie(jugador.Nombre, recordJugadorMaquina);
            var recordMaquina = GetJugadorFromCookie("RMaquina");
            if (recordJugadorMaquina.Record() > recordMaquina.Record())
                SetJugadorForCookie("RMaquina", recordJugadorMaquina);
            Juego.Instance.UpdateScorePlayer(jugador, estado, restantes);
            Juego.Instance.UpdateRecordSist();
        }
    }
}