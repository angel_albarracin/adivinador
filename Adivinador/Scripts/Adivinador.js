﻿var numero_secreto = Math.floor((Math.random() * 100) + 1);
var intentos = 0;
var intentados = " ";

$(document).ready(function () {
    $("#secret").html(numero_secreto);
});

function intentar() {
    let num = $("#numero").val();
    if (validarNum(num)) {
        intentos++;
        intentados += num + " ";
        resultado(num);
        $("#intentos").html(intentados);
    }
}

function validarNum(num) {
    let valido = true;
    if (num == "") {
        alert("Debe ingresar un valor");
        valido = false;
    }
    if ( (num < 1 || num > 100) && valido) {
        alert("El valor ingresado debe estar entre 1 y 100");
        valido = false;
    }
    return valido;
}

function resultado(num) {
    if (num == numero_secreto)
        $("#resultado").html("Ganaste");
    if (intentados == 10) {
        $("#resultado").html("Perdiste");
    }
}